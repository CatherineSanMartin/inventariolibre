<%-- 
    Document   : index
    Created on : 04-05-2020, 0:26:12
    Author     : cathe
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>EVA3</title>
    </head>
    <body style="background-image: url(imagen/imagen1.png)">
       
        
        <h1> Evaluación 3 </h1>
        <h1> Alumna: Catherine San Martin Paves </h1>
        <h1> Taller de Aplicaciones Empresariales</h1>
        
        
        <br>
        <P>CRUD
        Es fundamental para las aplicaciones persistentes en sistemas de bases de datos 
        ya que son requeridas por el usuario para crear y gestionar datos, el CRUD es el
        acronimo de cuatro operaciones que se describen a continuación:</p>
        </br>
       
        <br><h5>Create (POST): se crea un nuevo registro en el inventario</h5>
        <br/>
        <br>
        <h5>http://LAPTOP-6A061ALD:8080/inventariolibre-rest-1.0-SNAPSHOT/api/iventariolibre</h5> 
        </br>
        
        <br>
        <h5>Read: donde podemos ver los registros</h5>
        </br>
        
        <br>
        <h5>Listar(GET): Aqui podemos ver la lista de productos que tiene el inventario.</h5>
        </br>
        <br>
        <h5>http://LAPTOP-6A061ALD:8080/inventariolibre-rest-1.0-SNAPSHOT/api/iventariolibre</h5>
        </br>
        
        <br>
        <h5>Buscar(GET): Podemos buscar cualquier ID que este dentro de la base de datos</h5>
        </br>
        <br>
        <h5>http://LAPTOP-6A061ALD:8080/inventariolibre-rest-1.0-SNAPSHOT/api/iventariolibre/2</h5>
        </br>
        
        <br>
        <h5>Update(PUT): Podemos actualizar nuestros productos</h5>
        </br> 
        <br>
        <h5>http://LAPTOP-6A061ALD:8080/inventariolibre-rest-1.0-SNAPSHOT/api/iventariolibre</h5>
        </br>
        
        <br>
        <h5>Delete(DELETE): Eliminar nuestros registro</h5>
        </br>
        <br>
        <h5>http://LAPTOP-6A061ALD:8080/inventariolibre-rest-1.0-SNAPSHOT/api/iventariolibre/4</h5>
        </br>
        
    </body>
</html>
