package root.services;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import root.persistence.entities.Inventariolibre;

@Path("/iventariolibre")
public class inventarioRest {

    EntityManagerFactory emf = Persistence.createEntityManagerFactory("inventario_PU");
    EntityManager em;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response listarTodo() {
        em = emf.createEntityManager();
        List<Inventariolibre> lista = em.createNamedQuery("Inventariolibre.findAll").getResultList();
        return Response.ok(200).entity(lista).build();
    }

    @GET
    @Path("/{idbuscar}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response buscarId(@PathParam("idbuscar") String idbuscar) {

        em = emf.createEntityManager();
        Inventariolibre inventario = em.find(Inventariolibre.class, idbuscar);
        return Response.ok(200).entity(inventario).build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public String nuevo(Inventariolibre Inventariolibrenuevo) {

        em = emf.createEntityManager();
        em.getTransaction().begin();
        em.persist(Inventariolibrenuevo);
        em.getTransaction().commit();
        return "Producto Guardado";

    }

    @PUT
    public Response actualizar(Inventariolibre inventarioUpdate) {
        String resultado = null;

        em = emf.createEntityManager();
        em.getTransaction().begin();
        inventarioUpdate = em.merge(inventarioUpdate);
        em.getTransaction().commit();
        return Response.ok(inventarioUpdate).build();

    }
    
    @DELETE
    @Path("/{iddelete}")
    @Produces({MediaType.TEXT_PLAIN, MediaType.APPLICATION_JSON})
    public Response eliminaId(@PathParam("iddelete") String iddelete){
       em = emf.createEntityManager();
       em.getTransaction().begin();
       Inventariolibre inventarioEliminar = em.getReference(Inventariolibre.class, iddelete);
       em.remove(inventarioEliminar);
       em.getTransaction().commit();
       return Response.ok("Producto Eliminado").build();
   }

}
