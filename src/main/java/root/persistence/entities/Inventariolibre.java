/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.persistence.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author cathe
 */
@Entity
@Table(name = "inventariolibre")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Inventariolibre.findAll", query = "SELECT i FROM Inventariolibre i"),
    @NamedQuery(name = "Inventariolibre.findById", query = "SELECT i FROM Inventariolibre i WHERE i.id = :id"),
    @NamedQuery(name = "Inventariolibre.findByFactura", query = "SELECT i FROM Inventariolibre i WHERE i.factura = :factura"),
    @NamedQuery(name = "Inventariolibre.findByCodproduct", query = "SELECT i FROM Inventariolibre i WHERE i.codproduct = :codproduct"),
    @NamedQuery(name = "Inventariolibre.findByProducto", query = "SELECT i FROM Inventariolibre i WHERE i.producto = :producto"),
    @NamedQuery(name = "Inventariolibre.findByPreciocosto", query = "SELECT i FROM Inventariolibre i WHERE i.preciocosto = :preciocosto"),
    @NamedQuery(name = "Inventariolibre.findByPreciocomercial", query = "SELECT i FROM Inventariolibre i WHERE i.preciocomercial = :preciocomercial"),
    @NamedQuery(name = "Inventariolibre.findByStock", query = "SELECT i FROM Inventariolibre i WHERE i.stock = :stock"),
    @NamedQuery(name = "Inventariolibre.findBySucursal", query = "SELECT i FROM Inventariolibre i WHERE i.sucursal = :sucursal"),
    @NamedQuery(name = "Inventariolibre.findByRegion", query = "SELECT i FROM Inventariolibre i WHERE i.region = :region")})
public class Inventariolibre implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "id")
    private String id;
    @Size(max = 2147483647)
    @Column(name = "factura")
    private String factura;
    @Size(max = 2147483647)
    @Column(name = "codproduct")
    private String codproduct;
    @Size(max = 2147483647)
    @Column(name = "producto")
    private String producto;
    @Size(max = 2147483647)
    @Column(name = "preciocosto")
    private String preciocosto;
    @Size(max = 2147483647)
    @Column(name = "preciocomercial")
    private String preciocomercial;
    @Size(max = 2147483647)
    @Column(name = "stock")
    private String stock;
    @Size(max = 2147483647)
    @Column(name = "sucursal")
    private String sucursal;
    @Size(max = 2147483647)
    @Column(name = "region")
    private String region;

    public Inventariolibre() {
    }

    public Inventariolibre(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFactura() {
        return factura;
    }

    public void setFactura(String factura) {
        this.factura = factura;
    }

    public String getCodproduct() {
        return codproduct;
    }

    public void setCodproduct(String codproduct) {
        this.codproduct = codproduct;
    }

    public String getProducto() {
        return producto;
    }

    public void setProducto(String producto) {
        this.producto = producto;
    }

    public String getPreciocosto() {
        return preciocosto;
    }

    public void setPreciocosto(String preciocosto) {
        this.preciocosto = preciocosto;
    }

    public String getPreciocomercial() {
        return preciocomercial;
    }

    public void setPreciocomercial(String preciocomercial) {
        this.preciocomercial = preciocomercial;
    }

    public String getStock() {
        return stock;
    }

    public void setStock(String stock) {
        this.stock = stock;
    }

    public String getSucursal() {
        return sucursal;
    }

    public void setSucursal(String sucursal) {
        this.sucursal = sucursal;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Inventariolibre)) {
            return false;
        }
        Inventariolibre other = (Inventariolibre) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "root.persistence.entities.Inventariolibre[ id=" + id + " ]";
    }
    
}
